﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp136
{
    class Program
    {
        static void Main(string[] args)
        {
             Console.WriteLine("Exercies :: to separate the individual characters from a string ");
            Console.WriteLine(new string('_', 30));
            Console.Write("Input string - ");
            string a = Convert.ToString(Console.ReadLine());
            foreach (char i in a)
            {
                Console.Write($"{i} ");
            }
        }
    }
}
